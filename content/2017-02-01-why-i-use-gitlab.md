+++
title = "Why I Use GitlabCI"
template = "post.html"
aliases = ["/blog/why-i-use-gitlab/"]

[extra]
excerpt = "For years, Github had a near monopoly on source code hosting; but Gitlab is offering real competition. Here's why I've made the switch."
+++
I've started moving all my projects away from Github in favor of Gitlab, and I haven't looked back.

I should say upfront that I'm not affiliated with nor employed by Gitlab. I haven't been paid, prompted, or asked by Gitlab to write this post. And, as far as I know, Gitlab has no idea I've written about them.

## My favorite Gitlab Features

 * Unlimited free closed projects, like Bitbucket
 * Painless Github importer
 * Remove source branch after merge to master automatically
 * Merge branch when tests pass automatically
 * Create a branch from a issue in website; auto-resolve issue when branch is merged
 * Integrated approach to software development makes it easier see what's happening in my project
 * Excellent issue tracker / basic project management
 * Integrated CI with excellent documentation
 * Proper docker support in CI (not a forked build of docker)
 * Builtin Docker image registry
 * Build environment can be any docker image
 * Parallel builds in CI
 * API for CI
 * CI Deployments track where your code is deployed to (I use this feature to render previews of my blog on S3 on a per branch basis)

## Gitlab is more innovative

Gitlab has pioneered a plethora of new features before Github, Bitbucket, Gogs or any of the other competitors in the space.  Gitlab's commitment to open source _product direction_, incident response, and source code means that I have insight into the product roadmap as well as the ability to contribute my feedback and features.

By comparison, Github has more-or-less copied a few of the best project management features but still lags behind Gitlab in terms of basic features like "Merge on Test Pass" and "Remove Source Branch on Merge" features. Github itself is 100% closed source and notorious for its lack of transparency in terms of product direction. Github eventually added the ability to rebase-merge, and create issue templates months and months after Gitlab. By choosing to use Gitlab, you're getting the best new features right away.

## GitlabCI is competitive

Let's talk about GitlabCI. Here's a quick comparison (due to age poorly) between the major continuous integration and continuous deployment options in the space:

CI/CD Service              | GitlabCI | TravisCI | CircleCI | Codeship | DroneCI | Jenkins
---------------------------|----------|----------|----------|----------|---------|--------
VCS Support                | git      | git      | git      | git      | git     | any via plugin
Github Support             | no, but..| yes      | yes      | yes      | Yes     | plugin
Gitlab Support             | gitlab   | no       | no       | yes      | No      | plugin
Bitbucket Support          | no, but..| no       | yes      | yes      | Yes     | plugin
Open Source                | Yes      | Yes      | No       | No?      | Yes     | Yes
Founded                    | 2011     | 2011     | 2011     | 2011     | ???     | 2004 (as Hudson)
Documentation Style        | Reference| Tutorial | Tutorial | Tutorial |Reference| Tutorial
Documentation Search       | No       | No       | Yes      | Yes      | No      | No
Cloud Option               | Yes      | Yes      | Yes      | Yes      | No      | CloudBees
On-premise Option          | CE & EE  | No       | EE       | No       | Yes     | Yes
Local execution            | Yes      | Maybe... | No       | Yes      | Maybe...| Docker
Debugging                  | Local    | SSH      | SSH      | Local    | Web     | No
Linux Test env             | Yes      | Yes      | Yes      | Yes      | Yes     | Yes
OSX Test env               | Not Cloud| Yes      | Yes      | Not Cloud| No      | Yes
Windows Test env           | Not Cloud| No       | No       | Not Cloud| No      | Yes
Docker support             | BEST EVER| Yes      | Awful    | Yes      | Yes     | Yes
Enterprise Support         | Yes      | Yes      | Yes      | Yes      | No      | Probably
Free Closed Source Projects| Yes      | No       | No       | 1 project| On prem | On prem
Build API                  | Yes      | No       | Yes      | Yes      | No      | Yes
Manual Test                | Yes      | No       | No       | No       | No      | No
Source Controlled Config   | Yes      | Yes      | Yes      | Yes      | No      | No
Secrets management         | Web      | YML & Web| Web      |Web&docker| Web     | Web
Artifacts                  | Native   | S3       | Native   | S3/SFTP  |S3/Heroku| plugins
Parallel Build "Steps"     | Yes      | Yes      | Yes      | Yes      | No      | No
Parallel step rerun        | No       | Yes      | No       | No       | No      | No
Parallel synchronization   | Yes      | No       | Yes      | Yes      | No      | No
Build notifications        | Yes      | Yes      | Yes      | Yes      | Yes     | Yes
Integrated Docker Registry | Yes      | No       | No       | No       | No      | No
Deployment targets         | Yes      | 3rd party| 3rd party| 3rd Party|3rd party| plugins
Badges                     | Yes      | Yes      | Yes      | Yes      | Yes     | plugin
Integrated Webhooks        | Yes      | No       | No       | Yes      | No      | No

One of the key differentiators above is the ability to run the CI locally. This is really really handy for two reasons:

 * Increased fault tolerance: even if your remote CI infrastructure is down, you can still run builds and deployments.
 * Debugging: its potentially much faster to iterate on a broken test locally

Both GitlabCI and Codeship are designed to help with the second bullet point. GitlabCI also provides the increased fault tolerance that I often wanted when I relied on CircleCI.

For me, GitlabCI is the most exciting CI option available, especially in the build-on-mac-and-deploy-to-linux scenario.

## Summary

Gitlab unifies all (or most) of the different services you previously relied on when using Github, but is flexible enough to work with other systems (via triggers, API, and webhooks) or simply does a better job than Github + other services. If you're thinking about making the switch, I'd encourage you to try out the gitlab.com for your next side project or open source project.
