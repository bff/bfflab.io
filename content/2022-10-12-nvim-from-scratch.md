+++
title = "How I Nvim-from-Scratch"
template = "post.html"

[extra]
excerpt = "I've recently switched from my bespoke nvim setup to nvim-from-scratch and found the transition difficult. This post summarizes key workflows and keybindings I use frequently to help me learn and memorize them."
+++
In this post, I'm going to list out key actions I try to take as headings on the page, and then write the keystrokes necessary to perform each one. It's designed to be a cheatsheet for navigating around files and projects quickly using [nvim-from-scratch](https://github.com/LunarVim/Neovim-from-scratch).

## nvim-tree Actions

I've been a loyal NerdTree plugin user for almost a decade, but nvim-from-scratch leverages a new file explorer plugin called [nvim-tree](https://github.com/nvim-tree/nvim-tree.lua/). I refer to this plugin as the "file explorer" below. All of these tips are things you can do using the nvim-tree plugin.

### Highlight Current Buffer's File in File Explorer

`<leader>e` in normal mode from a file buffer

### Copy and Rename a File from File Explorer

`cpr` while hovering over file in file explorer

### Open File in Vertical Split from File Explorer

`<C-v>` while hovering over file in file explorer

### Open File in Horizontal Split from File Explorer

`<C-x>` while hovering on file in file explorer

### Open File in Tab from File Explorer

`<C-t>` while hovering on file in file explorer

### Previous/Next Uncommitted Change in File Explorer

 * Next: `]c`
 * Previous: `[c`

### Toggle Hidden Files

 * dot files: `H`
 * git ignored files: `I`

### Move Up a Directory in File Explorer

`-`

### Close File Explorer

`q`

### More Default keybindings

See [nvim-tree](https://github.com/nvim-tree/nvim-tree.lua/blob/b01e7beaa6f0dbbf5df775cf4ecc829a23f0be54/doc/nvim-tree-lua.txt#L1240-L1241) repo

## LSP Actions

### goto definition

`gd` from normal mode while hovering on symbol

### goto references

`gr` from normal mode while hovering on symbol

## Telescope Actions

### Fuzzy Search Files by Name

`<leader>f`

## Ripgrep

### Populate quickfix window with ripgrep search results

`\` from normal mode

## vim-rhubarb

Didn't know this existed but now can't live without it:

### Open current buffer on Github.com in browser

`:GBrowse`

## Living Doc

My intention is to update this doc as I learn more shortcuts worth remembering.
