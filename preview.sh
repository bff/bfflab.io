#!/bin/bash -eu

echo "OUTDATED! UPDATE FOR ZOLA"
exit 2

declare CONTAINER_NAME='jekyll'
declare PORT=4000
declare URL="http://127.0.0.1:$PORT/"

echo "Starting jekyll in the background..."
docker run -d \
    -v "$(pwd):/src" \
    --name "$CONTAINER_NAME" \
    -p "$PORT:$PORT" \
  grahamc/jekyll \
    serve -H 0.0.0.0 2>/dev/null \
|| echo "Already running"

# Hacky workaround to make script Ubuntu and MacOS compatible
if [[ $(which xdg-open) ]]; then
  xdg-open "$URL"
elif [[ $(which open) ]]; then
  open "$URL"
else
  echo "Open this url in a browser: $URL"
fi
