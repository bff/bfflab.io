from subprocess import CalledProcessError
from unittest.mock import MagicMock

import pytest
from pytest import fixture

from bin.put_certificate import main, ApiError, TlsBundle


def test_main__fails_on_letsencrypt(acme_server, gitlab_server):
    # Given
    acme_server.no_route53_creds()
    argv = ['bin/put_certificate.py', 'example.com', 'your.mom@gmail.com', '1234']

    # Then
    with pytest.raises(ApiError, match='LetsEncrypt'):
        # When
        result = main(argv)


def test_main__fails_on_gitlab_403(acme_server, gitlab_server, tls_files):
    # Given
    acme_server.success()
    gitlab_server.unauthorized()
    argv = ['bin/put_certificate.py', 'example.com', 'your.mom@gmail.com', '1234']

    # Then
    with pytest.raises(ApiError, match='Gitlab'):
        # When
        main(argv)


def test_main__early_abort_without_gitlab_token():
    # Given
    argv = ['bin/put_certificate.py', 'example.com', 'your.mom@gmail.com', '1234']

    # Then
    with pytest.raises(ApiError, match='GITLAB_TOKEN'):
        # When
        main(argv)


def test_main__happy_path(acme_server, gitlab_server, tls_files):
    # Given
    acme_server.success()
    gitlab_server.success()
    argv = ['bin/put_certificate.py', 'example.com', 'your.mom@gmail.com', '1234']

    # When
    result = main(argv)

    # Then
    assert 'Success' in result


def test_main__parses_args(acme_server, gitlab_server, tls_files):
    # Given
    acme_server.success()
    gitlab_server.success()
    argv = ['bin/put_certificate.py', 'hi.mom', 'imjim@hotmail.com', '987654321']

    # When
    main(argv)

    # Then
    assert acme_server.domain == 'hi.mom'
    assert acme_server.email == 'imjim@hotmail.com'
    assert gitlab_server.domain == 'hi.mom'
    assert gitlab_server.project_id == '987654321'


@fixture
def acme_server(monkeypatch):
    acme_server = AcmeServerMock()
    monkeypatch.setattr('bin.put_certificate.subprocess', acme_server)
    return acme_server


class AcmeServerMock:
    """ LetsEncrypt server is called Acme """

    CalledProcessError = CalledProcessError

    def no_route53_creds(self):
        self.returncode = 1
        self.stdout = b'''Saving debug log to /var/log/letsencrypt/letsencrypt.log
                          Plugins selected: Authenticator dns-route53, Installer None
                          Obtaining a new certificate
                          Performing the following challenges:
                          dns-01 challenge for fisher-fleig.me
                          Cleaning up challenges

                          IMPORTANT NOTES:
                           - Your account credentials have been saved in your Certbot
                             configuration directory at /etc/letsencrypt. You should make a
                             secure backup of this folder now. This configuration directory will
                             also contain certificates and private keys obtained by Certbot so
                             making regular backups of this folder is ideal.'''
        self.stderr = b'''Unable to locate credentials
                          To use certbot-dns-route53, configure credentials as described at https://boto3.readthedocs.io/en/latest/guide/configuration.html#best-practices-for-configuring-credentials and add the necessary permissions for Route53 access.'''

    def success(self):
        self.returncode = 0
        self.stderr = b''
        self.stdout = b''

    def run(self, args, timeout=None, check=True):
        """ This replaces subprocess.run() inside CertbotRunner """

        # Set these values here for making assertions on later
        self.domain = args[8]
        self.email = args[5]

        if check and self.returncode != 0:
            raise CalledProcessError(self.returncode,
                                     ' '.join(args).encode(),
                                     stderr=self.stderr,
                                     output=self.stdout)


@fixture
def gitlab_server(monkeypatch):
    mock = GitlabServerMock()
    monkeypatch.setenv('GITLAB_TOKEN', 'superduperclassified!')
    monkeypatch.setattr('bin.put_certificate.requests', mock)
    return mock


class GitlabServerMock:

    def unauthorized(self):
        self.status_code = 403

    def success(self):
        self.status_code = 200

    def put(self, url, headers=None, data=None):
        """ Monkeypatched replacement for requests.put() """

        # Try to extract these values from url for making assertions on
        self.project_id = url.split('/')[6]
        self.domain = url.split('/')[9]

        response = MagicMock()
        response.status_code = self.status_code
        return response


@fixture
def tls_files(monkeypatch):
    tls_bundle = TlsBundle(MagicMock(), MagicMock())
    monkeypatch.setattr('bin.put_certificate.CertbotRunner.tls_bundle', lambda _: tls_bundle)
    return tls_bundle
